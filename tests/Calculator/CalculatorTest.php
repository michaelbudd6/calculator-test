<?php
declare(strict_types=1);

use App\Calculator\Calculator;
use App\Calculator\Models\CalculationInput;
use App\Calculator\Models\CalculationValue;
use App\Calculator\Operations\Add;
use App\Calculator\Operations\BitwiseAnd;
use App\Calculator\Operations\BitwiseOr;
use App\Calculator\Operations\Divide;
use App\Calculator\Operations\Minus;
use App\Calculator\Operations\Multiply;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

final class CalculatorTest extends TestCase
{
    /**
     * @var Calculator
     */
    private $calculator;

    public function setUp()
    {
        parent::setUp();

        $this->calculator = new Calculator(
            new Add(),
            new Minus(),
            new Multiply(),
            new Divide(),
            new BitwiseAnd(),
            new BitwiseOr(),
            new CalculationValue(0)
        );
    }

    /**
     * @test
     *
     * @return void
     */
    public function testAdditionOperation()
    {
        $this->calculator->add(new CalculationInput(25));
        $this->calculator->add(new CalculationInput(42));
        $this->calculator->add(new CalculationInput(32.21232));

        $this->assertEquals(
            new CalculationValue(99.21232),
            $this->calculator->currentValue()
        );
    }

    /**
     * @test
     *
     * @return void
     */
    public function testSubtractOperation()
    {
        $this->calculator->add(new CalculationInput(100));

        $this->calculator->subtract(new CalculationInput(32));
        $this->calculator->subtract(new CalculationInput(28.22));

        $this->assertEquals(
            new CalculationValue(39.78),
            $this->calculator->currentValue()
        );
    }

    /**
     * @test
     *
     * @return void
     */
    public function testMultiplicationOperation()
    {
        $this->calculator->add(new CalculationInput(33));

        $this->calculator->multiply(new CalculationInput(27.3));

        $this->assertEquals(
            new CalculationValue(900.9),
            $this->calculator->currentValue()
        );
    }

    /**
     * @test
     *
     * @expectedException App\Calculator\Exceptions\IllegalCalculationInput
     *
     * @return void
     */
    public function testDivisionByZero()
    {
         // todo: decide upon behaviour

        $this->calculator->add(new CalculationInput(27));

        $this->calculator->divide(new CalculationInput(0));

        $this->assertEquals(
            new CalculationValue(0),
            $this->calculator->currentValue()
        );
    }

    /**
     * @test
     *
     * @return void
     */
    public function testDivisionOperation()
    {
        $this->calculator->add(new CalculationInput(42));

        $this->calculator->divide(new CalculationInput(5.6));

        $this->assertEquals(
            new CalculationValue(7.5),
            $this->calculator->currentValue()
        );
    }

    /**
     * @test
     *
     * @return void
     */
    public function testBitwiseAndOperation()
    {
        $this->calculator->add(new CalculationInput(7));

        $this->calculator->and(new CalculationInput(55));

        $this->assertEquals(
            new CalculationValue(7),
            $this->calculator->currentValue()
        );
    }

    /**
     * @test
     *
     * @return void
     */
    public function testBitwiseOrOperation()
    {
        $this->calculator->add(new CalculationInput(7));

        $this->calculator->or(new CalculationInput(55));

        $this->assertEquals(
            new CalculationValue(55),
            $this->calculator->currentValue()
        );
    }

    /**
     * @test
     *
     * @return void
     */
    public function testCalculationWithMultipleOperationTypes()
    {
        $this->calculator->add(new CalculationInput(23));
        $this->calculator->subtract(new CalculationInput(7));
        $this->calculator->multiply(new CalculationInput(3));
        $this->calculator->divide(new CalculationInput(2));

        $this->assertEquals(
            new CalculationValue(24),
            $this->calculator->currentValue()
        );
    }
}
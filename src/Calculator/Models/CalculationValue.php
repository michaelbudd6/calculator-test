<?php
declare(strict_types=1);

namespace App\Calculator\Models;

use App\Calculator\Models\Interfaces\CalculationValue as CalculationValueInterface;

final class CalculationValue implements CalculationValueInterface
{
    /**
     * @var float
     */
    private $value;

    /**
     * @param float $value
     */
    public function __construct(float $value = 0)
    {
        $this->value = $value;
    }

    /**
     * @return float
     */
    public function value(): float
    {
        return $this->value;
    }
}

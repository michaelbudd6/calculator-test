<?php
declare(strict_types=1);

namespace App\Calculator\Models;

use App\Calculator\Models\Interfaces\CalculationInput as CalculationInputInterface;

final class CalculationInput implements CalculationInputInterface
{
    /**
     * @var float
     */
    private $value;

    /**
     * @param float $value
     */
    public function __construct(float $value)
    {
        $this->value = $value;
    }

    /**
     * @return float
     */
    public function value(): float
    {
        return $this->value;
    }

    /**
     * @param CalculationInputInterface $input
     *
     * @return bool
     */
    public function equals(CalculationInputInterface $input): bool
    {
        return $input->value() === $this->value;
    }
}

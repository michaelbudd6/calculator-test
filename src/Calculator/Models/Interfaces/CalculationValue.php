<?php
declare(strict_types=1);

namespace App\Calculator\Models\Interfaces;

interface CalculationValue
{
    public function value(): float;
}

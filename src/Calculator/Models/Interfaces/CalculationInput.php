<?php
declare(strict_types=1);

namespace App\Calculator\Models\Interfaces;

interface CalculationInput
{
    /**
     * @return float
     */
    public function value(): float;

    /**
     * @param CalculationInput $input
     *
     * @return bool
     */
    public function equals(CalculationInput $input): bool;
}

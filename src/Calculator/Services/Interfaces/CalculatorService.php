<?php
declare(strict_types=1);

namespace App\Calculator\Services\Interfaces;

use App\Calculator\Models\Interfaces\CalculationValue;
use Symfony\Component\HttpFoundation\Request;

interface CalculatorService
{
    /**
     * @param Request $request
     *
     * @return CalculationValue
     */
    public function runFromRequest(Request $request): CalculationValue;
}

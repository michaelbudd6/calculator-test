<?php
declare(strict_types=1);

namespace App\Calculator\Services;

use App\Calculator\Interfaces\Calculator;
use App\Calculator\Models\CalculationInput;
use App\Calculator\Models\CalculationValue;
use App\Calculator\Models\Interfaces\CalculationInput as CalculationInputInterface;
use App\Calculator\Models\Interfaces\CalculationValue as CalculationValueInterface;
use App\Calculator\Operations\Add;
use App\Calculator\Operations\BitwiseAnd;
use App\Calculator\Operations\BitwiseOr;
use App\Calculator\Operations\Divide;
use App\Calculator\Operations\Minus;
use App\Calculator\Operations\Multiply;
use App\Calculator\Services\Interfaces\CalculatorService as CalculatorServiceInterface;
use Symfony\Component\HttpFoundation\Request;

final class CalculatorService implements CalculatorServiceInterface
{
    /**
     * @var Calculator
     */
    private $calculator;

    /**
     * @param Calculator $calculator
     */
    public function __construct(Calculator $calculator)
    {
        $this->calculator = $calculator;
    }

    /**
     * @param Request $request
     *
     * @return CalculationValueInterface
     */
    public function runFromRequest(Request $request): CalculationValueInterface
    {
        $operations  = $request->get('operation');
        $inputValues = $request->get('inputValue');

        collect($inputValues)
            ->map(static::mapToCalculationInputs())
            ->filter(static::filterNonUsedInputs())
            ->each(function (CalculationInputInterface $input, $i) use ($operations) {
                switch ($operations[$i]) {
                    case Add::OPERATION_TYPE:
                        $this->calculator->add($input);
                        break;
                    case BitwiseAnd::OPERATION_TYPE:
                        $this->calculator->and($input);
                        break;
                    case BitwiseOr::OPERATION_TYPE:
                        $this->calculator->or($input);
                        break;
                    case Divide::OPERATION_TYPE:
                        $this->calculator->divide($input);
                        break;
                    case Minus::OPERATION_TYPE:
                        $this->calculator->subtract($input);
                        break;
                    case Multiply::OPERATION_TYPE:
                        $this->calculator->multiply($input);
                        break;
                }
            });

        return $this->calculator->currentValue();
    }

    /**
     * @return \Closure
     */
    private static function filterNonUsedInputs(): \Closure
    {
        return function ($input): bool {
            return !empty($input);
        };
    }

    /**
     * @return \Closure
     */
    private static function mapToCalculationInputs(): \Closure
    {
        return function (string $input): ?CalculationInputInterface {
            if (trim($input) === '') {
                return null;
            }

            return new CalculationInput((float) $input);
        };
    }
}

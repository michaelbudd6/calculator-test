<?php
declare(strict_types=1);

namespace App\Calculator\Exceptions;

final class IllegalCalculationInput extends \Exception
{
    private const DIVISION_BY_ZERO_MESSAGE = 'Cannot divide number by zero';

    /**
     * @return IllegalCalculationInput
     */
    public static function divideByInputValueCannotBeZero(): IllegalCalculationInput
    {
        return new static(static::DIVISION_BY_ZERO_MESSAGE);
    }
}

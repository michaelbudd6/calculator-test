<?php
declare(strict_types=1);

namespace App\Calculator\Operations;

use App\Calculator\Models\CalculationValue;
use App\Calculator\Models\Interfaces\CalculationInput;
use App\Calculator\Models\Interfaces\CalculationValue as CalculationValueInterface;
use App\Calculator\Operations\Interfaces\BitwiseAnd as BitwiseAndInterface;

final class BitwiseAnd implements BitwiseAndInterface
{
    public const OPERATION_TYPE = 'and';

    /**
     * {@inheritdoc}
     */
    public function calculate(
        CalculationInput $input,
        CalculationValueInterface $currentValue
    ): CalculationValueInterface {
        $result = $input->value() & $currentValue->value();

        return new CalculationValue($result);
    }
}

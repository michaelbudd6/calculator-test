<?php
declare(strict_types=1);

namespace App\Calculator\Operations;

use App\Calculator\Models\CalculationValue;
use App\Calculator\Models\Interfaces\CalculationInput;
use App\Calculator\Models\Interfaces\CalculationValue as CalculationValueInterface;
use App\Calculator\Operations\Interfaces\Multiply as MultiplyInterface;

final class Multiply implements MultiplyInterface
{
    public const OPERATION_TYPE = 'multiply';

    /**
     * {@inheritdoc}
     */
    public function calculate(
        CalculationInput $input,
        CalculationValueInterface $currentValue
    ): CalculationValueInterface {
        $result = $input->value() * $currentValue->value();

        return new CalculationValue($result);
    }
}

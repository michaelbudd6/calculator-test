<?php
declare(strict_types=1);

namespace App\Calculator\Operations;

use App\Calculator\Models\CalculationValue;
use App\Calculator\Models\Interfaces\CalculationInput;
use App\Calculator\Models\Interfaces\CalculationValue as CalculationValueInterface;
use App\Calculator\Operations\Interfaces\Minus as MinusInterface;

final class Minus implements MinusInterface
{
    public const OPERATION_TYPE = 'minus';

    /**
     * {@inheritdoc}
     */
    public function calculate(
        CalculationInput $input,
        CalculationValueInterface $currentValue
    ): CalculationValueInterface {
        $result = $currentValue->value() - $input->value();

        return new CalculationValue($result);
    }
}

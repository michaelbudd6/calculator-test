<?php
declare(strict_types=1);

namespace App\Calculator\Operations;

use App\Calculator\Exceptions\IllegalCalculationInput;
use App\Calculator\Models\CalculationInput;
use App\Calculator\Models\CalculationValue;
use App\Calculator\Models\Interfaces\CalculationInput as CalculationInputInterface;
use App\Calculator\Models\Interfaces\CalculationValue as CalculationValueInterface;
use App\Calculator\Operations\Interfaces\Divide as DivideInterface;

final class Divide implements DivideInterface
{
    public const OPERATION_TYPE = 'divide';

    /**
     * {@inheritdoc}
     *
     * @throws IllegalCalculationInput
     */
    public function calculate(
        CalculationInputInterface $input,
        CalculationValueInterface $currentValue
    ): CalculationValueInterface {
        $this->guardInput($input);

        $result = $currentValue->value() / $input->value();

        return new CalculationValue($result);
    }

    /**
     * @param CalculationInputInterface $input
     *
     * @return void
     *
     * @throws IllegalCalculationInput
     */
    private function guardInput(CalculationInputInterface $input): void
    {
        if ($input->equals(new CalculationInput(0))) {
            throw IllegalCalculationInput::divideByInputValueCannotBeZero();
        }
    }
}

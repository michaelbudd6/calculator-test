<?php
declare(strict_types=1);

namespace App\Calculator\Operations\Interfaces;

interface Minus extends Operation
{
}

<?php
declare(strict_types=1);

namespace App\Calculator\Operations\Interfaces;

use App\Calculator\Models\Interfaces\CalculationInput;
use App\Calculator\Models\Interfaces\CalculationValue;

interface Operation
{
    /**
     * @param CalculationInput $input
     * @param CalculationValue $currentValue
     *
     * @return CalculationValue
     */
    public function calculate(CalculationInput $input, CalculationValue $currentValue): CalculationValue;
}

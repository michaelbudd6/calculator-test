<?php
declare(strict_types=1);

namespace App\Calculator\Operations;

use App\Calculator\Models\CalculationValue;
use App\Calculator\Models\Interfaces\CalculationInput;
use App\Calculator\Models\Interfaces\CalculationValue as CalculationValueInterface;
use App\Calculator\Operations\Interfaces\Add as AddInterface;

final class Add implements AddInterface
{
    public const OPERATION_TYPE = 'add';

    /**
     * {@inheritdoc}
     */
    public function calculate(CalculationInput $input, CalculationValueInterface $existingValue): CalculationValueInterface
    {
        $result = $input->value() + $existingValue->value();

        return new CalculationValue($result);
    }
}

<?php
declare(strict_types=1);

namespace App\Calculator\Interfaces;

use App\Calculator\Models\Interfaces\CalculationInput;
use App\Calculator\Models\Interfaces\CalculationValue;

interface Calculator
{
    /**
     * @param CalculationInput $input
     */
    public function add(CalculationInput $input): void;

    /**
     * @param CalculationInput $input
     */
    public function subtract(CalculationInput $input): void;

    /**
     * @param CalculationInput $input
     */
    public function divide(CalculationInput $input): void;

    /**
     * @param CalculationInput $input
     */
    public function multiply(CalculationInput $input): void;

    /**
     * @return CalculationValue
     */
    public function currentValue(): CalculationValue;

    /**
     * @param CalculationInput $input
     */
    public function and(CalculationInput $input): void;

    /**
     * @param CalculationInput $input
     */
    public function or(CalculationInput $input): void;
}

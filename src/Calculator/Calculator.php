<?php
declare(strict_types=1);

namespace App\Calculator;

use App\Calculator\Interfaces\Calculator as CalculatorInterface;
use App\Calculator\Models\Interfaces\CalculationInput;
use App\Calculator\Models\Interfaces\CalculationValue;
use App\Calculator\Operations\BitwiseAnd;
use App\Calculator\Operations\BitwiseOr;
use App\Calculator\Operations\Interfaces\Add;
use App\Calculator\Operations\Interfaces\Divide;
use App\Calculator\Operations\Interfaces\Minus;
use App\Calculator\Operations\Interfaces\Multiply;

final class Calculator implements CalculatorInterface
{
    /**
     * @var Add
     */
    private $addOperation;

    /**
     * @var CalculationValue
     */
    private $calculationValue;

    /**
     * @var Minus
     */
    private $minusOperation;

    /**
     * @var Multiply
     */
    private $multiplyOperation;

    /**
     * @var Divide
     */
    private $divideOperation;

    /**
     * @var BitwiseAnd
     */
    private $bitwiseAndOperation;

    /**
     * @var BitwiseOr
     */
    private $bitwiseOrOperation;

    /**
     * @param Add              $addOperation
     * @param Minus            $minusOperation
     * @param Multiply         $multiplyOperation
     * @param Divide           $divideOperation
     * @param BitwiseAnd       $bitwiseAndOperation
     * @param BitwiseOr        $bitwiseOrOperation
     * @param CalculationValue $calculationValue
     */
    public function __construct(
        Add $addOperation,
        Minus $minusOperation,
        Multiply $multiplyOperation,
        Divide $divideOperation,
        BitwiseAnd $bitwiseAndOperation,
        BitwiseOr $bitwiseOrOperation,
        CalculationValue $calculationValue
    ) {
        $this->addOperation        = $addOperation;
        $this->calculationValue    = $calculationValue;
        $this->minusOperation      = $minusOperation;
        $this->multiplyOperation   = $multiplyOperation;
        $this->divideOperation     = $divideOperation;
        $this->bitwiseAndOperation = $bitwiseAndOperation;
        $this->bitwiseOrOperation  = $bitwiseOrOperation;
    }

    /**
     * {@inheritdoc}
     */
    public function add(CalculationInput $input): void
    {
        $this->calculationValue = $this->addOperation->calculate($input, $this->calculationValue);
    }

    /**
     * {@inheritdoc}
     */
    public function subtract(CalculationInput $input): void
    {
        $this->calculationValue = $this->minusOperation->calculate($input, $this->calculationValue);
    }

    /**
     * {@inheritdoc}
     */
    public function divide(CalculationInput $input): void
    {
        $this->calculationValue = $this->divideOperation->calculate($input, $this->calculationValue);
    }

    /**
     * {@inheritdoc}
     */
    public function multiply(CalculationInput $input): void
    {
        $this->calculationValue = $this->multiplyOperation->calculate($input, $this->calculationValue);
    }

    /**
     * {@inheritdoc}
     */
    public function currentValue(): CalculationValue
    {
        return $this->calculationValue;
    }

    /**
     * {@inheritdoc}
     */
    public function and(CalculationInput $input): void
    {
        $this->calculationValue = $this->bitwiseAndOperation->calculate($input, $this->calculationValue);
    }

    /**
     * {@inheritdoc}
     */
    public function or(CalculationInput $input): void
    {
        $this->calculationValue = $this->bitwiseOrOperation->calculate($input, $this->calculationValue);
    }
}

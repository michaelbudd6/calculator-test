<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class Calculate extends AbstractController
{
    /**
     * @Route("/", name="calculate")
     *
     * @return Response
     */
    public function index()
    {
        return $this->render('calculator/index.html.twig', ['result' => 0]);
    }
}

<?php
declare(strict_types=1);

namespace App\Controller;

use App\Calculator\Services\Interfaces\CalculatorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

final class CalculationResult extends AbstractController
{
    /**
     * @Route("/calculate-result", name="calculate-result", methods={"POST"})
     *
     */
    public function index(Request $request, CalculatorService $calculatorService)
    {
        $result = $calculatorService->runFromRequest($request);

        return $this->render('calculator/index.html.twig', ['result' => $result->value()]);
    }
}
